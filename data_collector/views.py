import os

import requests
from django.conf import settings
from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse

from data_collector.models import Human, Planet, TransformedData

from data_collector.utils import api_get_all_users, api_get_all_planets


def main(request):
    return render(request, 'data_collector/main.html', {
        'transformed_data': TransformedData.objects.all().order_by('transformed_at')
    })


def fetch_data(request):
    """
    This action is getting data from api and save in database
    """
    people_response = requests.get(settings.SWAPI_PEOPLE_ENDPOINT)
    homeworld_response = requests.get(settings.SWAPI_HOMEWORLD_ENDPOINT)

    if people_response.status_code == 200 and homeworld_response.status_code == 200:
        # Clear database - we dont want to have too much data in db for efficiency
        Planet.objects.all().delete()
        Human.objects.all().delete()

        # Create Planets in database
        planet_count = api_get_all_planets(homeworld_response.json())

        # Create People in database and assign homeworld to them
        human_count = api_get_all_users(people_response.json())

        # Save this transformed data into database for history
        from data_collector.transformer import transform_and_save

        trans_record = TransformedData.objects.create()
        # Transform data and save to appropriate file
        transform_and_save(trans_record.get_file_name())

        messages.success(request, 'Created {} people and {} planets'.format(
            human_count,
            planet_count
        ))
    else:
        messages.error(request, 'Api is not available at this moment. Please try again later')

    return redirect(reverse('main'))


def download_file(request, pk):
    from django.core.files.storage import FileSystemStorage
    from django.http import FileResponse

    file = get_object_or_404(TransformedData, pk=pk)
    fs = FileSystemStorage(settings.MEDIA_ROOT)
    response = FileResponse(fs.open(file.get_file_name(), 'rb'), content_type='application/force-download')
    response['Content-Disposition'] = 'attachment; filename="{}"'.format(file.get_file_name())
    return response


def explore_data(request, pk):
    transformed_data = get_object_or_404(TransformedData, pk=pk)

    rows_limit = int(request.GET.get('limit', 10))
    increase_limit = 10

    import petl as etl
    data_table = etl.fromcsv(os.path.join(settings.MEDIA_ROOT, transformed_data.get_file_name()))
    # Table headers
    data_table_headers = etl.header(data_table)
    available_filters = data_table_headers

    # Filters for aggregation
    is_filtering = False
    filters = []
    for header in available_filters:
        if request.GET.get(header, None):
            is_filtering = True
            filters.append(header)

    if is_filtering:
        fields = tuple(filters) if len(filters) > 1 else str(filters[0])

        # Aggregating data
        data_table = etl.aggregate(data_table, fields, aggregation={
            'counter': len
        })

        # Change table headers automatically
        data_table_headers = etl.header(data_table)

    else:
        # Limit data_table if aggregation is not active
        data_table = etl.head(data_table, n=rows_limit)

    return render(request, 'data_collector/explore_data.html', {
        'transformed_data': transformed_data,
        'available_filters': available_filters,
        'filters': filters,
        'data_table': etl.data(data_table),
        'data_table_headers': data_table_headers,
        'rows_limit': rows_limit,
        'next_limit': rows_limit + increase_limit,
        'is_filtering': is_filtering
    })
