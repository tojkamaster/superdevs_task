import dateutil.parser
import requests

from data_collector.models import Planet, Human


def parse_api_url_and_get_number(url: str) -> int:
    """
    This function parse url and get ID from API URL.
    """
    return int(url.split('/')[-2])


def api_get_all_planets(homeworld_response) -> int:
    # Create Planets in database
    planet_count = 0
    planet_page = 0

    fetching_continue = True
    while fetching_continue:
        if homeworld_response['next'] is None:
            fetching_continue = False

        for planet in homeworld_response['results']:
            created_planet = Planet.objects.create(
                pk=(parse_api_url_and_get_number(planet['url'])),
                name=planet['name'],
                rotation_period=planet['rotation_period'],
                orbital_period=planet['orbital_period'],
                diameter=planet['diameter'],
                climate=planet['climate'],
                gravity=planet['gravity'],
                terrain=planet['terrain'],
                surface_water=planet['surface_water'],
                population=planet['population']
            )

            planet_count += 1

        planet_page += 1
        if fetching_continue:
            homeworld_response = requests.get(homeworld_response['next']).json()

    return planet_count


def api_get_all_users(people_response) -> int:
    # Create People in database
    human_count = 0
    human_page = 0

    fetching_continue = True
    while fetching_continue:
        if people_response['next'] is None:
            fetching_continue = False

        for human in people_response['results']:
            planet = Planet.objects.get(pk=parse_api_url_and_get_number(human['homeworld']))

            created_planet = Human.objects.create(
                pk=(parse_api_url_and_get_number(human['url'])),
                name=human['name'],
                height=human['height'],
                mass=human['mass'],
                hair_color=human['hair_color'],
                skin_color=human['skin_color'],
                eye_color=human['eye_color'],
                gender=human['gender'],
                homeworld=planet,
                birth_year=human['birth_year'],
                edited=dateutil.parser.isoparse(human['edited']).strftime('%Y-%m-%d %H:%M:%S')
            )
            human_count += 1

        human_page += 1
        if fetching_continue:
            people_response = requests.get(people_response['next']).json()

    return human_count
