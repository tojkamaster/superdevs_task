from django.db import models


class Human(models.Model):
    name = models.CharField(max_length=255, null=False)
    height = models.CharField(max_length=255, null=False)
    mass = models.CharField(max_length=255, null=True)
    hair_color = models.CharField(max_length=255, null=False)
    skin_color = models.CharField(max_length=255, null=False)
    eye_color = models.CharField(max_length=255, null=False)
    gender = models.CharField(max_length=255, null=False)
    homeworld = models.ForeignKey(
        'Planet',
        null=True,
        on_delete=models.SET_NULL
    )
    birth_year = models.CharField(max_length=255, null=False)
    edited = models.DateTimeField(null=False)

    class Meta:
        db_table = 'human'


class Planet(models.Model):
    name = models.CharField(max_length=255, null=False)
    rotation_period = models.CharField(max_length=255, null=False)
    orbital_period = models.CharField(max_length=255, null=False)
    diameter = models.CharField(max_length=255, null=False)
    climate = models.CharField(max_length=255, null=False)
    gravity = models.CharField(max_length=255, null=False)
    terrain = models.CharField(max_length=255, null=False)
    surface_water = models.CharField(max_length=255, null=False)
    population = models.CharField(max_length=255, null=False)

    class Meta:
        db_table = 'planet'


class TransformedData(models.Model):
    transformed_at = models.DateTimeField(auto_now_add=True)

    def get_file_name(self):
        return self.transformed_at.strftime('%Y_%m_%d_%H_%M_%S.csv')

    class Meta:
        db_table = 'transformed_data'
