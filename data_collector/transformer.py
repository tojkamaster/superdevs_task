import os

from django.conf import settings
from django.db import connection
import petl as etl


def transform_and_save(filename: str):
    people_dataset = etl.fromdb(connection, "SELECT * FROM human")
    planet_dataset = etl.fromdb(connection, "SELECT * FROM planet")

    # Prepare planet dataset - get only one column and rename it
    planet_dataset = etl.cut(planet_dataset, 'id', 'name')
    planet_dataset = etl.rename(planet_dataset, 'name', 'homeworld')

    # Rename fields in datasets
    people_transformed = etl.rename(people_dataset, 'edited', 'date')

    # Convert date for valid format
    people_transformed = etl.convert(people_transformed, 'date', lambda v: v.date())

    # Join 2 datasets and get homeworld name
    people_transformed = etl.join(people_transformed, planet_dataset, lkey='homeworld_id', rkey='id')

    # Get only columns what are needed
    people_transformed = etl.cut(
        people_transformed, 'name', 'height', 'mass', 'hair_color', 'skin_color', 'eye_color',
        'gender', 'birth_year', 'date', 'homeworld'
    )

    etl.tocsv(people_transformed, os.path.join(settings.MEDIA_ROOT, filename))
