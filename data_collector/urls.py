from django.urls import path

from data_collector import views

urlpatterns = [
    path('', views.main, name='main'),
    path('fetch_data/', views.fetch_data, name='fetch_data'),
    path('download_file/<int:pk>/', views.download_file, name='download_file'),
    path('explore_data/<int:pk>/', views.explore_data, name='explore_data'),
]
